/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@apiglobal/test-sdk',
  version: '2.0.2',
  description: 'an sdk for testing ag handlers'
}
