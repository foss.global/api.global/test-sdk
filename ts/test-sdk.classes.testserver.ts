import * as plugins from './test-sdk.plugins.js';

export class AgTestServer {
  public handlers: Array<plugins.agSdk.AAgHandler<any>> = [];
  public server: plugins.smartexpress.Server;

  constructor(handlerArg?: plugins.agSdk.AAgHandler<any>) {
    if (handlerArg) {
      this.addAgHandler(handlerArg);
    }
  }

  public async addAgHandler(handlerArg: plugins.agSdk.AAgHandler<any>) {
    this.handlers.push(handlerArg);
    console.log(`added handler with slug ${handlerArg.slug}`);
    if (this.server && this.server.serverStatus === 'running') {
      await this.stop();
      await this.start();
    }
  }

  public async start() {
    this.server = new plugins.smartexpress.Server({
      cors: true,
      forceSsl: false,
      defaultAnswer: async () => 'apiglobal testserver',
      domain: 'localhost',
      port: 3000,
    });
    for (const handlerArg of this.handlers) {
      console.log(`found handler with slug ${handlerArg.slug}`);
      await handlerArg.start();
      console.log(`started handler with slug ${handlerArg.slug}`);
      const slugroute = `/${handlerArg.slug}`;
      this.server.addRoute(
        slugroute,
        new plugins.smartexpress.HandlerTypedRouter(handlerArg.typedrouter)
      );
      console.log(`added slugroute ${slugroute}`);
    }
    await this.server.start();
  }

  public async stop() {
    if (this.server) {
      const previousServer = this.server;
      this.server = null;
      await previousServer.startedPromise;
      await previousServer.stop();
    }
  }
}
