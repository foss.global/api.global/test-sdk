import * as plugins from './test-sdk.plugins.js';
import { AgTestServer } from './test-sdk.classes.testserver.js';
import { AAgHandler } from '@apiglobal/sdk';

export { AgTestServer };

let testServer: AgTestServer;
let handler: AAgHandler<any>;
export const createTestServer = async (handlerArg: {
  new (envManagerArg: plugins.agSdk.AgEnvironment): plugins.agSdk.AAgHandler<any>;
}) => {
  class AgEnvironement extends plugins.agSdk.AgEnvironment {
    public qenv = new plugins.qenv.Qenv('./', './.nogit');
    public async getEnvVar(nameArg: string) {
      return this.qenv.getEnvVarOnDemand(nameArg);
    }
  }
  handler = new handlerArg(new AgEnvironement());
  console.log(`now checking requirements for handler with slug ${handler.slug}...`);
  await handler.checkRequirements();
  testServer = new AgTestServer(handler);
  await testServer.start();
  return testServer;
};

export const testFire = <
  A extends plugins.agSdk.AAgHandler<any>,
  T extends plugins.typedrequestInterfaces.ITypedRequest
>(
  slugArg: A['slug'],
  methodArg: T['method'],
  requestArg: T['request']
) => {
  if (!testServer) {
    throw new Error('you need to create and start a testServer first!');
  }
  if (testServer.server.serverStatus !== 'running') {
    throw new Error('you need to start the testServer first!');
  }
  const typedRequest = new plugins.typedrequest.TypedRequest<T>(
    `http://localhost:${testServer.server.options.port}/${slugArg}`,
    methodArg
  );
  const responsePromise = typedRequest.fire(requestArg);
  const expect = async (expectedResponseArg: T['response']) => {
    const actualResponse = await responsePromise;
    const comparisonResult = plugins.smartobject.compareObjects(
      expectedResponseArg,
      actualResponse
    );
    let throwErrorBool = false;
    if (comparisonResult.divergingProperties.length > 0) {
      console.log(`The following properties diverged:`);
      console.log(comparisonResult.divergingProperties);
      for (const divProperty of comparisonResult.divergingProperties) {
        if (expectedResponseArg[divProperty] !== 'maydiverge') {
          throwErrorBool = true;
        } else {
          console.log(`${divProperty} may diverge, not throwing for this one`);
        }
      }
    }
    if (comparisonResult.missingProperties.length > 0) {
      console.log(`The following properties diverged:`);
      console.log(comparisonResult.divergingProperties);
      throwErrorBool = true;
    }
    if (throwErrorBool) {
      console.log('Result: response did not comply');
      console.log('-> expected:');
      console.log(expectedResponseArg);
      console.log('-> but actually received:');
      console.log(actualResponse);
      throw new Error('response did not comply');
    }
    return actualResponse;
  };
  return {
    expect,
  };
};

export const stopTestServer = async () => {
  if (testServer) {
    await testServer.stop();
    await handler.stop();
  }
};
